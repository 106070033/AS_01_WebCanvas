# Software Studio 2020 Spring
## Assignment 01 Web Canvas

106070033 蘇子軒
### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Ｙ         |

---


### How to use 

    Describe how to use your web and maybe insert images to help you explain.
![](https://i.imgur.com/Bb7oI3E.png)畫筆：一進去小畫家預設的功能，或是點這個圖案就可以畫了
![](https://i.imgur.com/Eo1bE0m.png)橡皮擦：點這個圖案就可以畫了
![](https://i.imgur.com/eLkDfFB.png)塗滿：在畫多邊形時點一下會填滿，再點一下解除
![](https://i.imgur.com/ZbxQ1v6.png)筆刷大小：控制筆刷和橡皮擦和多邊形的粗細
![](https://i.imgur.com/ABOunqh.png)畫三角形:以按下去的點為對稱點
![](https://i.imgur.com/Xq7Kx9b.png)畫長方形:以按下去的點為中心
![](https://i.imgur.com/gI5IUaL.png)畫圓形:以按下去的點為圓心
![](https://i.imgur.com/6ncAH0a.png)輸入文字：可調整字體大小和字型
![](https://i.imgur.com/Pf2XLcX.png)按一下回到前一步
![](https://i.imgur.com/chy4wQW.png)按一下回到後一步
![](https://i.imgur.com/83UGHF2.png)reset：重啟一片新畫布
![](https://i.imgur.com/Ws7J2LJ.png)上傳圖片
![](https://i.imgur.com/dMUpIiv.png)畫直線：點按兩下可以形成線，可以輸入想要折幾次
![](https://i.imgur.com/ACZjUCT.png)下載為png檔
![](https://i.imgur.com/O8kHjoI.png)彩虹線條使筆刷呈現彩虹


### Function description

    Decribe how to implement functions including bonus function and how to use it.
#### 滑鼠鍵盤 事件
![](https://i.imgur.com/vFrYHIy.png)
>將這些funciton 包在canvas的tag中

* function getMousePos(e) 
* function mousedown()
* function mouseup()
* function keyboard(event)

---

#### 畫畫 功能
![](https://i.imgur.com/96YnjcC.png)
>因為大部分功能都是滑鼠移動時改變，所以放在gesmousepos裡面，根據不同的畫畫模式（看drawing_flag而改變）

* function reset()
>重畫一塊白色的畫布，並使cStep歸零
* function brush()
>在eraser mode將globalCompositeOperation 設為 "destination-out"
>使滑鼠經過路線全部變成透明
>在rainbow mode使顏色變彩虹
* function drawrect()
>直接用strokeRect
* function drawtri()
>先用ctx.moveTo(dot_x,dot_y);
        ctx.lineTo(cur_x,cur_y);
        ctx.lineTo(dot_x*2-cur_x,cur_y);
        ctx.lineTo(dot_x,dot_y);
再用stroke
* function drawcir()

>先用arc(dot_x,dot_y,radius,0,Math.PI*2,false);
>再用stroke
* function draw_text(a,b,c)
>a是要寫入的文字b,c是座標
---

#### button 功能
![](https://i.imgur.com/3qIfhzg.png)
>按下按鈕時改變drawing_flag，並且切換cursor

![](https://i.imgur.com/cOcHxG3.png)
>thetype是各種不同的cursor檔名當作input
* function erase_button(theType)
* function brush_button(theType)
* function rectangle_button(theType)
* function circle_button(theType)
* function triangle_button(theType)
* function fill_button()
* function rainbow()
* function text_button()
 >創造一個新的input element，在輸入完成後將值傳入一個變數中，刪除此element，並用call draw_text()用strokeText畫出
* function polygon_button(theType)

---

#### 回復功能

* function cPush()
>將目前canvas的資料轉成URL並放入array中並以cstep來控制array位置
* function cUndo()
>cstep- -，並將目前畫面洗白後將array[cstep]資料畫入
* function cRedo()
>cstep+ +，並將目前畫面洗白後將array[cstep]資料畫入

---

#### 上傳下載功能
上網查的
* upload
寫在這裡，搜尋此funciton
```
//upload
document.getElementById('inp').onchange = function(e) {}
```
>將影像讀入後畫進canvas
* download
寫在這裡，搜尋此funciton
```
var download = function(){}
```
>下載為png檔


---

#### 取得 input 功能

將許多input還有下拉式選單的資料存在javascript的一些全域變數中之後比較好控制，使用一堆getelementById再assign給其他全域變數

還有改變cursor的funciton
![](https://i.imgur.com/Sv1park.png)


---

#### 顏色 功能
* 畫出漸層：
有找到一個東西叫lineargradient,可以有漸層的效果
並使用hsl控制顏色
從原點到（寬,0）做白色到所選顏色的漸層
從原點到（0,高）做透明到黑色的漸層
將兩個圖疊加起來就可以了
```
function color_selector() {
    ///current_hse是顏色
    var gradient = c_ctx.createLinearGradient(0,0,c_canvas.width,0);
    gradient.addColorStop(0, "white");
    gradient.addColorStop(1, `hsl(${current_hse}, 100%, 50%)`);
    var gradient2 = c_ctx.createLinearGradient(0,0,0,c_canvas.height);
    gradient2.addColorStop(0, "rgba(0,0,0,0)");
    gradient2.addColorStop(1, "black");
    c_ctx.fillStyle = gradient;
    c_ctx.fillRect(0, 0, c_canvas.width, c_canvas.height);
    c_ctx.fillStyle = gradient2;
    c_ctx.fillRect(0, 0, c_canvas.width, c_canvas.height);

}
```
* 點擊選顏色
在圖的範圍內點到的話就擷取起點是按下去的點長寬是1的範圍的顏色，再將其資料放入RGB
```
///onclick color
function select_c(){
    if((real_x>=947) && (real_y>=47) && (real_x<=1047) && (real_y<=147)){
            console.log("color!!");
            var temp = c_ctx.getImageData(real_x-947,real_y-47,1,1).data;
            color = "rgb("+temp[0]+","+temp[1]+","+temp[2]+")";
        }
}
```


---

#### 其他 功能
brush_shape:使用slider還有下拉式選單(brush_s())控制
```
var slider = document.getElementById("slider");
slider.oninput = function() {
  line_width = this.value;
}
```
在brush中會使
```
ctx.lineWidth = line_width;
```
藉此改變筆刷大小

---

#### bonus功能
* 彩虹功能-在按下彩虹後筆刷會變彩色再按一下結束
用rainbow()這個function控制rainbow_flag
然後在brush()中會有以下code來改變顏色
```
if(rainbow_flag){
    ctx.strokeStyle = `hsl(${hse}, 100%, 50%)`;
    if(hse<360)hse++;
    else hse = 0;
    console.log(hse+"rainbow");
}else ctx.strokeStyle = color.value;
```
使用hsl來控制顏色，因為hsl 0~360剛好是全部的顏色區，後面的100%,50%可能是亮度透明度之類的吧
* 多個直線-input輸入折的次數，點畫布連線（如圖）
![](https://i.imgur.com/nwiIZ9M.png)
>輸入3會有三條線並折兩次，但滑鼠移動時不會有線在預覽，就是點一下後直接畫出來
實作方式：用polygon_button('cursor/polygon.cur')這個function控制drawing_flag = 6
並在mousedown()時畫線並使得剩餘次數減一
![](https://i.imgur.com/NYzJ2X1.png)

### Gitlab page link

    your web page URL, which should be 
https://106070033.gitlab.io/AS_01_WebCanvas/

### Others (Optional)

    Anything you want to say to TAs.


<style>
table th{
    width: 100%;
}
</style>