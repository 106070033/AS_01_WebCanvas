/**
 * bug 1 ：press時畫的移出canvas外，之後不用press就能畫
 */
var cPushArray = new Array();
var cStep = -1;
var color =  "rgb(255,0,0)";
var current_hse = 0;
var saturation = "100%";
var lightness = "50%";
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var c_canvas = document.getElementById("ccccolor");
var c_ctx = c_canvas.getContext("2d");
var gradient = ctx.createLinearGradient(0, 0, 0, 40);
//var cursor = document.getElementById("frame");
///117/41
var canvas_edge_x = 50;
var canvas_edge_y = 90;
var canvas_width =830;
var canvas_height = 500;

var cur_x = 0;
var cur_y = 0;
var prev_x = 0;
var prev_y = 0;
var real_x = 0;
var real_y = 0;
var line_width = 10;
var drawing_flag = 0;///when mousedown
var mousedown_flag = false;
var eraser_flag = false;
var rainbow_flag = false;
var polygon_times = 0;
var press_two = 0;
var imageData;
var hse = 0;
//press point
var dot_x = 0;
var dot_y = 0;
var p_dot_x = 0;
var p_dot_y = 0;
//fill
var fill = false;
////text_input
var input;
var input_exist = false;
var font_typ = "Arial";
var font_siz = "30px";
//init
function init() {
    
    reset();
}


/////////////////////dealing with mouse
  function getMousePos(e) {
    //console.log(drawing_flag);
    //color_selector();
    prev_x = cur_x;
    prev_y = cur_y;
    cur_x = e.clientX-canvas_edge_x;
    cur_y = e.clientY-canvas_edge_y;
    real_x = e.clientX;
    real_y = e.clientY;
    var coor = "Coordinates: (" + cur_x + "," + cur_y + ")";
    var cool = "Coordinates: (" + e.clientX + "," + e.clientY + ")";
    console.log(coor);
    console.log(cool)
    //if((cur_x >0) && (cur_y>0) && (cur_x < canvas_width) && (cur_y < canvas_height))console.log("right")
    if(cStep === -1)cPush();
    if(mousedown_flag){
    switch (drawing_flag) {
        case 1:
            //document.getElementById("frame").style.cursor = "zoom";
            brush();
            break;
        case 2:
            //document.getElementById("frame").style.cursor = "auto";
            drawrect();
            break;
        case 3:
            //document.getElementById("frame").style.cursor = "wait";
            drawcir();
            break;
        case 4:
            //document.getElementById("frame").style.cursor = "crosshair";
            drawtri();
            break;
        case 5:
            //draw_text();
            break;
        case 6:
            //draw_polygon();
            break;
        default:
            break;
    }
}
    
}
///onclick color
function select_c(){
    console.log("color!!in");
    if((real_x>=947) && (real_y>=47) && (real_x<=1047) && (real_y<=147)){
        console.log("color!!");
            var temp = c_ctx.getImageData(real_x-947,real_y-47,1,1).data;
            color = "rgb("+temp[0]+","+temp[1]+","+temp[2]+")";
        }
}
    function mousedown(){
        ///之後要再判斷在canvas內///還沒做
        
        if((cur_x >0) && (cur_y>0) && (cur_x < canvas_width) && (cur_y < canvas_height)){
            mousedown_flag = true;
            if(drawing_flag === 0){
                drawing_flag = 1;
            }
            else if(drawing_flag === 5){
                dot_x = cur_x;
                dot_y = cur_y;
            }
            else if(drawing_flag === 6){
                dot_x = cur_x;
                dot_y = cur_y;
                if(press_two < 1+polygon_times)press_two++;
                if(press_two === 1){
                    p_dot_x = dot_x;
                    p_dot_y = dot_y;
                }
                else if(press_two >1){
                    ctx.strokeStyle = color;
                    ctx.fillStyle = color;
                    ctx.lineWidth = line_width;
                    ctx.lineJoin = ctx.lineCap = 'round';
                    ctx.moveTo(p_dot_x,p_dot_y);
                    ctx.lineTo(dot_x,dot_y);
                    if(!fill)ctx.stroke();
                    else ctx.fill();
                    ctx.closePath();
                    polygon_times--;
                    p_dot_x = dot_x;
                    p_dot_y = dot_y;
                }
                if(polygon_times ==0){
                    drawing_flag = 1;
                    press_two = 0;
                    document.getElementById("cur").style.cursor = "url(cursor/brush.cur),default";
                }
            }
            else if(drawing_flag >= 2){////rectangle
                imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                dot_x = cur_x;
                dot_y = cur_y;
            }
    }
     //console.log(mousedown_flag)
    }
    function mouseup(){
        
        //console.log("mouseup"+canvas.height+canvas.width+" "+mousedown_flag);
        if(cStep === -1)cPush();
        if((cur_x >0) && (cur_y>0) && (cur_x < canvas_width) && (cur_y < canvas_height)){
            mousedown_flag = false;
            //console.log("mouseup__");
            //drawing_flag = 0;  /////之後看要不要變？？ 
            ctx.globalCompositeOperation = "source-over";
            cPush();
            if(drawing_flag === 5){
                dot_x = cur_x;
                dot_y = cur_y;
                if(input_exist == false){
                    input = document.createElement('input');
                    input.type = 'text';
                    input.style.position = 'fixed';
                    input.style.left = cur_x+canvas_edge_x+ 'px';
                    input.style.top = cur_y+canvas_edge_y+'px';
                    document.body.appendChild(input);
                    input.focus();
                    input_exist = true;
                }
            }
        }
        // if(eraser_flag){
        //     drawing_flag = 0;
        //     ctx.globalCompositeOperation = "source-over"; 
        //     cPush();
        // }
        
    }
/////////////////////dealing with keyboard
    function keyboard(event){
    if(window.event) // IE
    {
        keynum = event.keyCode;
    }
    else if(event.which) // Netscape/Firefox/Opera
    {
        keynum = event.which
    }
    if (drawing_flag === 5) {
        if (keynum === 13) {
            draw_text(input.value, parseInt(input.style.left,10)-canvas_edge_x, parseInt(input.style.top,10)-canvas_edge_y+40);
            document.body.removeChild(input);
            input_exist = false;
            cPush();
        }
    }
        if(keynum === 38 )line_width++;
        else if(keynum === 40)line_width--;
        else if(keynum === 13)flash_flag = false;
        //console.log(line_width);
        //console.log(keynum);
    }
  ////////////function
  ///reset
  function reset(){
    //取消eraser
    color_selector();
    eraser_flag = false;
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    cStep = -1;
    cPush();
  }
  ///brush
  function brush(){
        if(eraser_flag && mousedown_flag){
            ctx.globalCompositeOperation = "destination-out";  
            ctx.beginPath();
            ctx.strokeStyle = color;
            ctx.lineWidth = line_width;
            ctx.moveTo(prev_x,prev_y);
            ctx.lineTo(cur_x,cur_y);
            ctx.closePath();
            ctx.lineJoin = ctx.lineCap = 'round';
            ctx.strokeStyle = "rgba(255,255,255,1)";
            ctx.stroke();
            ctx.fill;
            ctx.globalCompositeOperation = "source-over"; 
        }else if(!eraser_flag && mousedown_flag){
            ctx.beginPath();
            if(rainbow_flag){
                ctx.strokeStyle = `hsl(${hse}, 100%, 50%)`;
                if(hse<360)hse++;
                else hse = 0;
                console.log(hse+"rainbow");
            }
            else ctx.strokeStyle = color;
            ctx.lineWidth = line_width;
            ctx.moveTo(prev_x,prev_y);
            ctx.lineTo(cur_x,cur_y);
            ctx.lineJoin = ctx.lineCap = 'round';
            ctx.globalCompositeOperation = "source-over"; 
            ctx.stroke();
            ctx.closePath();
        }
  }
/////draw shape
function drawrect(){
    if(mousedown_flag){ 
        //cursor.cursor = url(/cursor/rect.png);
        ctx.putImageData(imageData, 0, 0);
        ctx.beginPath();
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        ctx.lineJoin = ctx.lineCap = 'round';
        ctx.lineWidth = line_width;
        if(!fill)ctx.strokeRect(Math.min(dot_x,cur_x), Math.min(dot_y,cur_y), Math.abs(dot_x-cur_x), Math.abs(dot_y-cur_y));
        else ctx.fillRect(Math.min(dot_x,cur_x), Math.min(dot_y,cur_y), Math.abs(dot_x-cur_x), Math.abs(dot_y-cur_y));
        ctx.closePath();
    }
}
function drawtri(){
    if(mousedown_flag){ 
        ctx.putImageData(imageData, 0, 0);
        ctx.beginPath();
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        ctx.lineWidth = line_width;
        ctx.lineJoin = ctx.lineCap = 'round';
        ctx.moveTo(dot_x,dot_y);
        ctx.lineTo(cur_x,cur_y);
        ctx.lineTo(dot_x*2-cur_x,cur_y);
        ctx.lineTo(dot_x,dot_y);
        if(!fill)ctx.stroke();
        else ctx.fill();
        ctx.closePath();
    }
}
function drawcir(){
    if(mousedown_flag){ 
        ctx.putImageData(imageData, 0, 0);
        ctx.beginPath();
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        ctx.lineWidth = line_width;
        ctx.lineJoin = ctx.lineCap = 'round';
        var radius = Math.sqrt((dot_x-cur_x)*(dot_x-cur_x) + (dot_y-cur_y)*(dot_y-cur_y) );
        ctx.arc(dot_x,dot_y,radius,0,Math.PI*2,false); // Outer circle
        if(!fill)ctx.stroke();
        else ctx.fill();
        ctx.closePath();
    }
}
function draw_text(a,b,c) {
        ctx.font = font_siz+" "+font_typ;
        ctx.lineWidth = 1;
        if(!fill)ctx.strokeText(a, b, c);
        else ctx.fillText(a,b,c);
}

/////button && cursor
function erase_button(theType){
    document.getElementById("cur").style.cursor = "url("+theType+"),default";
    eraser_flag = true;
    drawing_flag = 1;
}
function brush_button(){
    eraser_flag = false;
    rainbow_flag = false;
    drawing_flag = 1;
    if(rainbow_flag)document.getElementById("cur").style.cursor = "url(cursor/rainbow.cur),default";
    else document.getElementById("cur").style.cursor = "url(cursor/brush.cur),default";
}
function rectangle_button(theType){
    document.getElementById("cur").style.cursor = "url("+theType+"),default";
    drawing_flag = 2;
}
function circle_button(theType){
    document.getElementById("cur").style.cursor = "url("+theType+"),default";
    drawing_flag = 3;
}
function triangle_button(theType){
    document.getElementById("cur").style.cursor = "url("+theType+"),default";
    drawing_flag = 4;
}
function fill_button(){
    fill = !fill;
}
function rainbow(){
    drawing_flag = 1;
    eraser_flag = false;
    rainbow_flag = true;
    if(rainbow_flag)document.getElementById("cur").style.cursor = "url(cursor/rainbow.cur),default";
    else document.getElementById("cur").style.cursor = "url(cursor/brush.cur),default";
}
///text

function text_button(){
    document.getElementById("cur").style.cursor = "url(cursor/text.cur),default";
    drawing_flag = 5;
}
function polygon_button(theType){
    polygon_times =  document.getElementById("poly").value;
    console.log(polygon_times)
    document.getElementById("cur").style.cursor = "url("+theType+"),default";
    drawing_flag = 6;
}
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('canvas').toDataURL());
}     
function cUndo() {
    console.log(cStep);
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];   
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}      
function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}      
////slider
var slider = document.getElementById("slider");
slider.oninput = function() {
  line_width = this.value;
}
var c_slider = document.getElementById("c_slider")
setInterval(function(){
	current_hse = c_slider.value;
})

//upload
document.getElementById('inp').onchange = function(e) {
    var img = new Image();
    img.onload = function() {
        ctx.drawImage(this,6,6,canvas_width,canvas_height);
        cPush();
    }
    img.onerror = failed();
    img.src = URL.createObjectURL(this.files[0]);
    
  };
  function failed() {
    console.error("The provided file couldn't be loaded as an Image media");
  }
//////download
  var download = function(){
    var link = document.createElement('a');
    link.download = 'canvas.png';
    link.href = document.getElementById('canvas').toDataURL()
    link.click();
  }

  ///cursor
  function changeCursor(theType)
{
    document.getElementById("cur").style.cursor = theType;
}
//font &size
function font_t(){
    var q= document.getElementById('font_type').value;
    q=document.getElementById('font_type').options[
        document.getElementById('font_type').selectedIndex
    ].value;
    font_typ = q;
}
function font_s(){
    var p = document.getElementById('font_size').value;
    p=document.getElementById('font_size').options[
        document.getElementById('font_size').selectedIndex
    ].value;
    font_siz = p;
}
function brush_s(){
    var r = document.getElementById('brush_size').value;
    r=document.getElementById('brush_size').options[
        document.getElementById('brush_size').selectedIndex
    ].value;
    line_width = r;
}
////color
function color_selector() {
    ///current_hse是顏色
    var gradient = c_ctx.createLinearGradient(0,0,c_canvas.width,0);
    gradient.addColorStop(0, "white");
    gradient.addColorStop(1, `hsl(${current_hse}, 100%, 50%)`);
    var gradient2 = c_ctx.createLinearGradient(0,0,0,c_canvas.height);
    gradient2.addColorStop(0, "rgba(0,0,0,0)");
    gradient2.addColorStop(1, "black");
    c_ctx.fillStyle = gradient;
    c_ctx.fillRect(0, 0, c_canvas.width, c_canvas.height);
    c_ctx.fillStyle = gradient2;
    c_ctx.fillRect(0, 0, c_canvas.width, c_canvas.height);

}

///選顏色：https://html-color-codes.info/